import logging
import pathlib
import sys

from dataclasses import dataclass
from typing import Any, Sequence

import catboost as catb  # type: ignore
import joblib  # type: ignore
import transformers  # type: ignore

from knowledge_integrity.featureset import ContentFeatures, FeatureSource, get_features
from knowledge_integrity.revision import CurrentRevision

from readability.models.readability_bert.bert import (
    max_pooling,
    mean_pooling,
    min_pooling,
    predict_sentences_scores,
)
from readability.utils import pad_list, text2sentences, wikitext2text


MODEL_FEATURES = ["revision_sentences"]
PAD_N = 15
PAD_TOKEN = -1
SENTENCES_PREFIX = "sent_"


MODEL_VERSION: int = 2
DECISION_THRESHOLD = 0.5


@dataclass
class MultilingualReadabilityModel:
    model_version: int
    classifier: transformers.Pipeline
    fk_scorer: catb.CatBoostRegressor
    supported_wikis: list[str]


@dataclass
class ClassifyResult:
    prediction: bool
    probability: float
    fk_score: float


def _transformed_readability_features(features: dict[str, Any]) -> dict[str, Any]:
    text = wikitext2text(features["wikitext"])
    sentences = text2sentences(text, lang=features["wiki_db"][:2])
    return dict(
        revision_sentences=sentences,
    )


def extract_features(
    revision: CurrentRevision, features: Sequence[str]
) -> dict[str, Any]:
    # groups of features required for classification model
    content_features = ContentFeatures(revision)
    feature_sources = (FeatureSource(source=content_features),)
    return get_features(feature_sources, features, _transformed_readability_features)


def load_model(model_path: pathlib.Path) -> MultilingualReadabilityModel:
    # ensure that joblib can find the model in global symbols when unpickling
    sys.modules["__main__"].MultilingualReadabilityModel = MultilingualReadabilityModel  # type: ignore  # noqa: E501

    with open(model_path, "rb") as f:
        model: MultilingualReadabilityModel = joblib.load(f)

    if model.model_version != MODEL_VERSION:
        logging.warn(
            "Serialized model version %d does not match current model version %d",
            model.model_version,
            MODEL_VERSION,
        )
    return model


def classify(model: MultilingualReadabilityModel, revision: CurrentRevision) -> ClassifyResult:

    readability_features = extract_features(revision, MODEL_FEATURES)

    # preparing features for fk_scorer
    sentences_scores = predict_sentences_scores(model.classifier, readability_features["revision_sentences"])
    sentences_scores_processed = pad_list(sentences_scores, pad_token=PAD_TOKEN, n=PAD_N)
    revision_features = {SENTENCES_PREFIX + str(i): ss for i, ss in enumerate(sentences_scores_processed, 1)}
    revision_features["bert_score_mean"] = mean_pooling(sentences_scores)
    revision_features["bert_score_min"] = min_pooling(sentences_scores)
    revision_features["bert_score_max"] = max_pooling(sentences_scores)

    # predicting flesch-kincaid grade score
    fk_score = model.fk_scorer.predict([revision_features[f] for f in model.fk_scorer.feature_names_])

    return ClassifyResult(
        # convert numpy values to make them serializable
        prediction=bool(revision_features["bert_score_mean"] > DECISION_THRESHOLD),
        probability=float(revision_features["bert_score_mean"]),
        fk_score=float(fk_score),
    )
